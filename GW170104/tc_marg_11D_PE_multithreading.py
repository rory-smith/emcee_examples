import numpy as np
from scipy.misc import logsumexp
import lal
import lalsimulation
import emcee
from emcee import PTSampler
from emcee.utils import MPIPool
import cvxopt

import sys
import time

import mkl
mkl.set_num_threads(16)
print mkl.get_max_threads()

fmin = 20.
fmax = 1024.
deltaF = 1./4.

######## load in data from file

data_file = np.column_stack( np.loadtxt("./data/data-dumpH1-freqData.dat") )
data = data_file[1] + 1j*data_file[2]
psd_file = np.column_stack( np.loadtxt("./data/data-dumpH1-PSD.dat") )
psd = psd_file[1]

## preprocessing ##

B_linear = np.load("./ROQstuff/B_linear.npy")
fnodes_linear = np.load("./ROQstuff/fnodes_linear.npy")
B_quadratic = np.load("./ROQstuff/B_quadratic.npy")
fnodes_quadratic = np.load("./ROQstuff/fnodes_quadratic.npy")
all_nodes  = np.hstack((fnodes_linear, fnodes_quadratic))


B_lin_dot_h = deltaF*4*data.conjugate()*B_linear / psd
weights_linear_tc = len(data)*( np.fft.irfft(B_lin_dot_h, axis=1) + 1j*np.fft.irfft(1j*B_lin_dot_h, axis=1)).T 
#NFFT = np.max(weights_linear_tc.shape)
#weights_linear_tc = cvxopt.matrix(weights_linear_tc)
weights_quadratic =  deltaF*4*np.dot(1./psd, B_quadratic.T)
dd = 4*deltaF*np.vdot(data, data/psd)

ntemps = 8
nwalkers = 22
ndim = 11
nsteps = 24

def mc_eta_to_m1m2(mc, eta):
  # note m1 >= m2
  if eta <= 0.25 and eta > 0.:
	root = np.sqrt(0.25-eta)
  	fraction = (0.5+root) / (0.5-root)
  	m1 = mc * (pow(1+1.0/fraction,0.2) / pow(1.0/fraction,0.6))
  	m2 = mc * (pow(1+fraction,0.2) / pow(fraction,0.6))
  	return m1, m2
  else:
	return 1., 500.
 
def htilde_of_f(m1, m2, chi1L, chi2L, chip, thetaJ, alpha, dist, fplus, fcross, phi_c, fnodes_linear, fnodes_quadratic):

        fref = 20.
        dist = 1e6*lal.lal.PC_SI*dist


        H_lin = lalsimulation.SimIMRPhenomPFrequencySequence(fnodes_linear, chi1L, chi2L, chip, thetaJ,
        m1, m2, dist, alpha, phi_c, fref, 1, None)
	H_quad = lalsimulation.SimIMRPhenomPFrequencySequence(fnodes_quadratic, chi1L, chi2L, chip, thetaJ,
        m1, m2, dist, alpha, phi_c, fref, 1, None)

        hplus_lin = fplus*H_lin[0].data.data
        hcross_lin = fcross*H_lin[1].data.data

	htilde_lin = (hplus_lin + hcross_lin)

	hplus_quad = fplus*H_quad[0].data.data
	hcross_quad = fcross*H_quad[1].data.data
	_htilde_quad = hplus_quad + hcross_quad

	htilde_quad = _htilde_quad.conjugate() * _htilde_quad

        return htilde_lin, htilde_quad

def LALInferenceCubeToFlatPrior(r, x1, x2):

	return x1 + r * ( x2 - x1 );


def logPrior(x):


	mc = x[0]
        eta = x[1]
	chi1L = x[2]
	chi2L = x[3]
	chip = x[4]
	thetaJ = x[5]
        alpha = x[6]
        dist = x[7]
	fplus = x[8]
	fcross = x[9]
	phi_c = x[10]
	
        m1, m2 = mc_eta_to_m1m2(mc, eta)
        q = m1/m2

	if mc >= mc_min and mc <= mc_max and q <= q_max and q >= q_min and dist <= dist_max and dist >= dist_min and alpha >= angle_min and alpha <= angle_max and thetaJ >= angle_min and thetaJ <= angle_max and chi1L >= chi_min and chi1L <= chi_max and chi2L >= chi_min and chi2L <= chi_max and chip >= chi_min and chip <= chi_max and phi_c <= angle_max and phi_c >= angle_min and fplus >= antenna_min and fplus <= antenna_max and fcross >= antenna_min and fcross  <= antenna_max :	

		logprior = np.log(((m1+m2)*(m1+m2))/((m1-m2)*pow(eta,3.0/5.0)) )	#mc, eta contribution	
	
		return logprior
	else:
		return -np.inf

def logL(x, dd, weights_linear_tc, weights_quadratic, fnodes_linear,fnodes_quadratic):

	mc = x[0]
        eta = x[1]
        chi1L = x[2]
        chi2L = x[3]
        chip = x[4]
        thetaJ = x[5]
        alpha = x[6]
        dist = x[7]
        fplus = x[8]
        fcross = x[9]
        phi_c = x[10]

        m1, m2 = mc_eta_to_m1m2(mc, eta)
        q = m1/m2

        if mc >= mc_min and mc <= mc_max and q <= q_max and q >= q_min and dist <= dist_max and dist >= dist_min and alpha >= angle_min and alpha <= angle_max and thetaJ >= angle_min and thetaJ <= angle_max and chi1L >= chi_min and chi1L <= chi_max and chi2L >= chi_min and chi2L <= chi_max and chip >= chi_min and chip <= chi_max and phi_c <= angle_max and phi_c >= angle_min and fplus >= antenna_min and fplus <= antenna_max and fcross >= antenna_min and fcross  <= antenna_max :       
		

        	m1 *= lal.lal.MSUN_SI
        	m2 *= lal.lal.MSUN_SI
	
	        htilde_lin, htilde_quad = htilde_of_f(m1, m2, chi1L, chi2L, chip, thetaJ, alpha, dist, fplus, fcross, phi_c, fnodes_linear, fnodes_quadratic)
		#dh = 0
		#for i in range(len(weights_linear_tc)):
		#	dh += np.dot(weights_linear_tc[i], htilde_lin)
			
		#dh = np.sum(weights_linear_tc.T * htilde_lin[:,np.newaxis])#np.dot(weights_linear_tc, htilde_lin)
		#htilde_lin = cvxopt.matrix(htilde_lin)
		#dh_tc = cvxopt.matrix(np.zeros(NFFT, dtype=complex))
		#cvxopt.blas.gemv(weights_linear_tc,htilde_lin,dh_tc, trans='N')
		#dh = sum(dh_tc)
		dh = np.dot(weights_linear_tc, htilde_lin)
		hh = np.dot(htilde_quad, weights_quadratic)#np.sum(htilde_quad * weights_quadratic)#np.dot(htilde_quad, weights_quadratic)
		scores = ( -0.5* ( -2*dh.real ) )
	        logL = logsumexp( scores ).real - 0.5*( hh + dd ) + np.log(1./(2.*fmax))
		return logL.real

	else:
		return -np.inf

q_min, q_max = 1., 8.
mc_min, mc_max = 13., 40.
eta_min, eta_max = (q_min*q_max)/((q_min+q_max)**2.), 0.25
chi_min, chi_max = 0, 1.
angle_min, angle_max = 0., np.pi*2.
dist_min, dist_max = 500., 1500
antenna_min, antenna_max = 0, 1

eta0 = np.random.uniform(low=eta_min, high=eta_max, size=(ntemps, nwalkers, 1))
mc0 =  np.random.uniform(low=mc_min, high=mc_max, size=(ntemps, nwalkers, 1))
chi1L =  np.random.uniform(low=chi_min, high=chi_max, size=(ntemps, nwalkers, 1))
chi2L =  np.random.uniform(low=chi_min, high=chi_max, size=(ntemps, nwalkers, 1))
chip =  np.random.uniform(low=chi_min, high=chi_max, size=(ntemps, nwalkers, 1))
alpha = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))
thetaJ = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))
dist = np.random.uniform(low=dist_min, high=dist_max, size=(ntemps, nwalkers, 1))
fplus = np.random.uniform(low=antenna_min, high=antenna_max, size=(ntemps, nwalkers, 1))
fcross = np.random.uniform(low=antenna_min, high=antenna_max, size=(ntemps, nwalkers, 1))

phi_c = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))


p0 = np.array([mc0, eta0, chi1L, chi2L, chip, alpha, thetaJ, dist, fplus, fcross, phi_c])
p0 = np.reshape(p0, (ndim,ntemps, nwalkers))
p0 = np.swapaxes(p0, 2,1)
p0 = np.swapaxes(p0, 0,2)

betas = np.logspace(0, -ntemps, ntemps, base=10)


sampler = PTSampler(ntemps, nwalkers, ndim, logL, logPrior, loglargs=[dd, weights_linear_tc, weights_quadratic, fnodes_linear, fnodes_quadratic], betas=betas)

t1 = time.time()
(pos, lnprob, rstate) = sampler.run_mcmc(p0, nsteps)

t2=time.time()
(lnZ_pt, dlnZ_pt) = sampler.thermodynamic_integration_log_evidence(fburnin=0.5)
print "lnZ_pt = {} +/- {}".format(lnZ_pt, dlnZ_pt)
print "BF = {} ".format(lnZ_pt + 0.5*4*deltaF*np.vdot(data,data/psd))

print t2-t1
'''
import matplotlib
matplotlib.use('Agg')

import corner
samples = sampler.chain[0]
#print sampler.get_autocorr_time()
samples = samples[:, 500:, :].reshape(-1, ndim)
fig = corner.corner(samples, labels=["$m_c$", "$\eta$", "chi1L", "chi2L", "chip", "alpha", "thetaJ", "dist", "F+", "Fx", "phi_c"],
                      )
fig.savefig("triangle.png")
'''
