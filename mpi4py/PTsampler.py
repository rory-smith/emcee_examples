import numpy as np
import lal
import lalsimulation
import emcee
from emcee import PTSampler
from emcee.utils import MPIPool

import sys
import time

fmin = 10.
fmax = 512.
deltaF = 1./8.

######## load in data from file


data_file = np.column_stack( np.loadtxt("H1-freqDataWithInj.dat") )
data = data_file[1] + 1j*data_file[2]
psd_file = np.column_stack( np.loadtxt("H1-PSD.dat") )
psd = psd_file[1]
# data and psd start at 0Hz: remove data and psd below fmin
fmin_index = int(fmin/deltaF)
data = data[fmin_index:len(data)]
psd = psd[fmin_index:len(psd)]


ntemps = 32 
nwalkers = 5000
ndim = 2
nsteps = 10000


def mc_eta_to_m1m2(mc, eta):
  # note m1 >= m2
  if eta <= 0.25 and eta > 0.:
	root = np.sqrt(0.25-eta)
  	fraction = (0.5+root) / (0.5-root)
  	m1 = mc * (pow(1+1.0/fraction,0.2) / pow(1.0/fraction,0.6))
  	m2 = mc * (pow(1+fraction,0.2) / pow(fraction,0.6))
  	return m1, m2
  else:
	return 1., 500. 
def htilde_of_f(fmin, fmax, deltaF, m1, m2):

        fref = 20.
        chi1L = 0
        chi2L = 0
        chip = 0
        thetaJ = 0
        alpha = 0
        dist = 1e6*lal.lal.PC_SI*1000

        H = lalsimulation.SimIMRPhenomP(chi1L, chi2L, chip, thetaJ,
        m1, m2, dist, alpha, 0, deltaF, fmin, fmax, fref, 1, None)

        fmin_index = int(fmin / deltaF) # waveform generator computes waveforms from
                                        # 0Hz, so use this to get the wavefrom from fmin
        hplus = H[0].data.data[fmin_index:len(H[0].data.data)]
        hcross = H[1].data.data[fmin_index:len(H[1].data.data)]
        return hplus + hcross

def logPrior(x):


	mc = x[0]
        eta = x[1]
        m1, m2 = mc_eta_to_m1m2(mc, eta)
        q = m1/m2

	if mc >= 13. and mc <= 40. and q <= 9. and q >= 1.:
		#logprior = np.log(((m1+m2)*(m1+m2))/((m1-m2)*pow(eta,3.0/5.0)) )	#eta contribution	
		logprior = np.log(1./(40-13)) + np.log(1./(0.25-0.09))
		return logprior
	else:
		return -np.inf

def logL(x, data, psd, fmin, fmax, deltaF):

        mc = x[0]
        eta = x[1]

	m1, m2 = mc_eta_to_m1m2(mc, eta)

        q = m1/m2

	if mc >= 13. and mc <= 40. and q <= 9. and q >= 1.:

        	m1 *= lal.lal.MSUN_SI
        	m2 *= lal.lal.MSUN_SI
	
	        htilde = htilde_of_f(fmin, fmax, deltaF, m1, m2)
	
	        logL = -0.5 * ( 4*deltaF*np.vdot(data - htilde, (data - htilde)/psd)).real 

		return logL

	else:
		return -np.inf



eta0 = np.random.uniform(low=0.09, high=0.25, size=(ntemps, nwalkers, 1))
mc0 =  np.random.uniform(low=13., high=40., size=(ntemps, nwalkers, 1))
p0 = np.array([mc0, eta0])
p0 = np.reshape(p0, (ndim,ntemps, nwalkers))
p0 = np.swapaxes(p0, 2,1)
p0 = np.swapaxes(p0, 0,2)

betas = np.logspace(0, -ntemps, ntemps, base=10)

pool = MPIPool(loadbalance=True)

if not pool.is_master():
    pool.wait()
    sys.exit(0)

sampler = PTSampler(ntemps, nwalkers, ndim, logL, logPrior, loglargs=[data, psd, fmin, fmax, deltaF], betas=betas, pool=pool)
t1 = time.time()
(pos, lnprob, rstate) = sampler.run_mcmc(p0, nsteps)
#all_nans = np.isnan(lnprob)
#lnprob[all_nans] = -15
print np.isnan(lnprob).any()
t2=time.time()
pool.close()
(lnZ_pt, dlnZ_pt) = sampler.thermodynamic_integration_log_evidence(fburnin=0.5)
print "lnZ_pt = {} +/- {}".format(lnZ_pt, dlnZ_pt)
print "BF = {} ".format(lnZ_pt + 0.5*4*deltaF*np.vdot(data,data/psd))

print t2-t1

import matplotlib
matplotlib.use('Agg')

import corner
samples = sampler.chain[0]
samples = samples.reshape(-1,ndim)
fig = corner.corner(samples, labels=["$m_c$", "$\eta$"],
                      )
fig.savefig("triangle.png")

