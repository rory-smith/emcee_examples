import numpy as np
from scipy.misc import logsumexp
import lal
import lalsimulation
import emcee
from emcee import PTSampler
from emcee.utils import MPIPool

import sys
import time

fmin = 20.
fmax = 1024. 
deltaF = 1./4.

fseries = np.linspace(0, fmax, int(fmax/deltaF) + 1)

######## load in data from file


data_fileH1 = np.column_stack( np.loadtxt("./data/data-dumpH1-freqData.dat") )
dataH1 = data_fileH1[1] + 1j*data_fileH1[2]
dataH1 = dataH1[:int(fmax/deltaF)+1]
psd_fileH1 = np.column_stack( np.loadtxt("./data/data-dumpH1-PSD.dat") )
psdH1 = psd_fileH1[1][:int(fmax/deltaF)+1]


data_fileL1 = np.column_stack( np.loadtxt("./data/data-dumpL1-freqData.dat") )
dataL1 = data_fileL1[1] + 1j*data_fileL1[2]
dataL1 = dataL1[:int(fmax/deltaF)+1]
psd_fileL1 = np.column_stack( np.loadtxt("./data/data-dumpL1-PSD.dat") )
psdL1 = psd_fileL1[1][:int(fmax/deltaF)+1]

################################

ntemps = 8 
nwalkers =50
ndim = 12
nsteps = 2000







def mc_eta_to_m1m2(mc, eta):
  # note m1 >= m2
  if eta <= 0.25 and eta > 0.:
	root = np.sqrt(0.25-eta)
  	fraction = (0.5+root) / (0.5-root)
  	m1 = mc * (pow(1+1.0/fraction,0.2) / pow(1.0/fraction,0.6))
  	m2 = mc * (pow(1+fraction,0.2) / pow(fraction,0.6))
  	return m1, m2
  else:
	return 1., 500.
 
def htilde_of_f(fmin, fmax, deltaF, m1, m2, chi1L, chi2L, chip, thetaJ, alpha, dist, phi_c):

        fref = 20.
        dist = 1e6*lal.lal.PC_SI*dist

        H = lalsimulation.SimIMRPhenomP(chi1L, chi2L, chip, thetaJ,
        m1, m2, dist, alpha, 0, deltaF, fmin, fmax, fref, 1, None)

        hplus = H[0].data.data * np.exp(1j*np.pi*phi_c)
        hcross = H[1].data.data * np.exp(1j*np.pi*phi_c)


        return hplus, hcross

def LALInferenceCubeToFlatPrior(r, x1, x2):

	return x1 + r * ( x2 - x1 );


def logPrior(x):


	mc = x[0]
        eta = x[1]
	chi1L = x[2]
	chi2L = x[3]
	chip = x[4]
	thetaJ = x[5]
        alpha = x[6]
        dist = x[7]
	ra = x[8]
	dec = x[9]
	psi = x[10]
	phi_c = x[11]
	
        m1, m2 = mc_eta_to_m1m2(mc, eta)
        q = m1/m2

	if mc >= mc_min and mc <= mc_max and q <= q_max and q >= q_min and dist <= dist_max and dist >= dist_min and alpha >= angle_min and alpha <= angle_max and thetaJ >= angle_min and thetaJ <= angle_max and chi1L >= chi_min and chi1L <= chi_max and chi2L >= chi_min and chi2L <= chi_max and chip >= chi_min and chip <= chi_max and phi_c <= angle_max and phi_c >= angle_min and ra <= angle_max and ra >= angle_min and dec <= angle_max and dec >= angle_min and psi <= angle_max and psi >= angle_min :	

		logprior = np.log(((m1+m2)*(m1+m2))/((m1-m2)*pow(eta,3.0/5.0)) )	#mc, eta contribution	
	
		return logprior
	else:
		return -np.inf

def logL(x, dataH1, dataL1, psdH1, psdL1, fmin, fmax, deltaF):

	mc = x[0]
        eta = x[1]
        chi1L = x[2]
        chi2L = x[3]
        chip = x[4]
        thetaJ = x[5]
        alpha = x[6]
        dist = x[7]
        ra = x[8]
        dec = x[9]
        psi = x[10]
        phi_c = x[11]	

        m1, m2 = mc_eta_to_m1m2(mc, eta)
        q = m1/m2

	if mc >= mc_min and mc <= mc_max and q <= q_max and q >= q_min and dist <= dist_max and dist >= dist_min and alpha >= angle_min and alpha <= angle_max and thetaJ >= angle_min and thetaJ <= angle_max and chi1L >= chi_min and chi1L <= chi_max and chi2L >= chi_min and chi2L <= chi_max and chip >= chi_min and chip <= chi_max and phi_c <= angle_max and phi_c >= angle_min and ra <= angle_max and ra >= angle_min and dec <= angle_max and dec >= angle_min and psi <= angle_max and psi >= angle_min :	

        	m1 *= lal.lal.MSUN_SI
        	m2 *= lal.lal.MSUN_SI
	
	        hp, hc = htilde_of_f(fmin, fmax, deltaF, m1, m2, chi1L, chi2L, chip, thetaJ, alpha, dist, phi_c)
	
		# Hardcoded to be the time at the start of the data segment for G268556
		epoch = 1167559934.62

	        epoch_GPS = lal.lal.LIGOTimeGPS(epoch)
	        
	        
	        
	        gmst = lal.GreenwichMeanSiderealTime(epoch_GPS)
		######################
				
		IFO_cached = lal.CachedDetectors[lal.LALDetectorIndexLHODIFF]

	        timedelay_H = lal.TimeDelayFromEarthCenter(IFO_cached.location, ra, 
	                                           dec, epoch_GPS)

		F_plus_H, F_cross_H = lal.ComputeDetAMResponse(IFO_cached.response, ra, 
	                                           dec, psi, gmst)

	        htilde_H = F_plus_H * hp + F_cross_H * hc
	        
		IFO_cached = lal.CachedDetectors[lal.LALDetectorIndexLLODIFF]
	        timedelay_L = lal.TimeDelayFromEarthCenter(IFO_cached.location, ra, 
	                                           dec, epoch_GPS)
	        
	        F_plus_L, F_cross_L = lal.ComputeDetAMResponse(IFO_cached.response, ra, 
       	                                    dec, psi, gmst)
       		htilde_L = F_plus_L * hp + F_cross_L * hc
       		 

        	htilde_L *= np.exp(1j*np.pi*2*fseries*timedelay_L)
        	htilde_H *= np.exp(1j*np.pi*2*fseries*timedelay_H)
        	
		dh_H =  deltaF*4*dataH1.conjugate()*htilde_H / psdH1
		hh_H = deltaF*4.*np.sum( htilde_H.conjugate()*htilde_H/psdH1 ).real
		dd_H = deltaF*4.*np.sum( dataH1.conjugate()*dataH1/psdH1 ).real
        
		dh_L =  deltaF*4*dataL1.conjugate()*htilde_L / psdL1
                hh_L = deltaF*4.*np.sum( htilde_L.conjugate()*htilde_L/psdL1 ).real
                dd_L = deltaF*4.*np.sum( dataL1.conjugate()*dataL1/psdL1 ).real

		scores_H = len(dataH1)*( -0.5* ( -2*np.fft.irfft(dh_H)) )
        	scores_L = len(dataL1)*( -0.5* ( -2*np.fft.irfft(dh_L)) )
	        logL = logsumexp( scores_H + scores_L ).real - 0.5*( hh_H + hh_L + dd_H + dd_L ) + np.log(2./(2.*fmax))
	
		return logL

	else:
		return -np.inf

eta_min, eta_max = 0.09, 0.25
q_min, q_max = 1., 9.
mc_min, mc_max = 13., 40.
chi_min, chi_max = 0, 1.
angle_min, angle_max = 0., np.pi*2.
dist_min, dist_max = 100., 5000
antenna_min, antenna_max = 0, 1

eta0 = np.random.uniform(low=eta_min, high=eta_max, size=(ntemps, nwalkers, 1))
mc0 =  np.random.uniform(low=mc_min, high=mc_max, size=(ntemps, nwalkers, 1))
chi1L =  np.random.uniform(low=chi_min, high=chi_max, size=(ntemps, nwalkers, 1))
chi2L =  np.random.uniform(low=chi_min, high=chi_max, size=(ntemps, nwalkers, 1))
chip =  np.random.uniform(low=chi_min, high=chi_max, size=(ntemps, nwalkers, 1))
alpha = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))
thetaJ = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))
dist = np.random.uniform(low=dist_min, high=dist_max, size=(ntemps, nwalkers, 1))

phi_c = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))
ra = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))
dec = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))
psi = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))

p0 = np.array([mc0, eta0, chi1L, chi2L, chip, alpha, thetaJ, dist, ra, dec, psi, phi_c])
p0 = np.reshape(p0, (ndim,ntemps, nwalkers))
p0 = np.swapaxes(p0, 2,1)
p0 = np.swapaxes(p0, 0,2)

betas = np.logspace(0, -ntemps, ntemps, base=10)

pool = MPIPool(loadbalance=True)

if not pool.is_master():
    pool.wait()
    sys.exit(0)

sampler = PTSampler(ntemps, nwalkers, ndim, logL, logPrior, loglargs=[dataH1, dataL1, psdH1, psdL1, fmin, fmax, deltaF], betas=betas, pool=pool)
t1 = time.time()
(pos, lnprob, rstate) = sampler.run_mcmc(p0, nsteps)
#all_nans = np.isnan(lnprob)
#lnprob[all_nans] = -15
t2=time.time()
pool.close()
(lnZ_pt, dlnZ_pt) = sampler.thermodynamic_integration_log_evidence(fburnin=0.5)
print "lnZ_pt = {} +/- {}".format(lnZ_pt, dlnZ_pt)
print "BF = {} ".format(lnZ_pt + 0.5*4*deltaF*np.vdot(dataH1,dataH1/psdH1) + 0.5*4*deltaF*np.vdot(dataL1,dataL1/psdL1))

print t2-t1

import matplotlib
matplotlib.use('Agg')

import corner
samples = sampler.chain[0]
#print sampler.get_autocorr_time()
samples = samples[:, 50:, :].reshape(-1, ndim)
fig = corner.corner(samples, labels=["$m_c$", "$\eta$", "chi1L", "chi2L", "chip", "alpha", "thetaJ", "dist", "ra", "dec", "psi", "phi_c"],
                      )
fig.savefig("triangle.png")

