import numpy as np
import lalsimulation 
from emcee.utils import MPIPool
import lal
from scipy.misc import logsumexp
import sys

def htilde_of_f(params):

	m1, m2, chi1L, chi2L, chip, thetaJ, alpha, dist, phi_c = 20, 20, 0, 0, 0, 0, 0, 1000, 0#params[0], params[1], params[2], params[3], params[4],params[5], params[6], params[7], params[8]
	m1 *= lal.MSUN_SI
	m2 *= lal.MSUN_SI

	fmin = 20
	fmax = 512
        fref = 20.
        dist = 1e6*lal.lal.PC_SI*dist
	deltaF = 1./8.
        H = lalsimulation.SimIMRPhenomP(chi1L, chi2L, chip, thetaJ,
        m1, m2, dist, alpha, 0, deltaF, fmin, fmax, fref, 1, None)

        hplus = H[0].data.data * np.exp(1j*np.pi*2.*phi_c)
        hcross = H[1].data.data * np.exp(1j*np.pi*2.*phi_c)

	dh_H = hplus*hcross
	scores = np.fft.irfft(dh_H)

	logL = logsumexp( scores ).real 

        return logL 



params = np.abs(np.random.normal(size=[9,48,16]))

pool = MPIPool(loadbalance=True)

if not pool.is_master():
    pool.wait()
    sys.exit(0)


for i in range(100):
	
	print i
	pool.map(htilde_of_f, params.reshape(-1, 9))

pool.close()
