import numpy as np
import lal
import lalsimulation
import emcee
from emcee import PTSampler
from emcee.utils import MPIPool

import sys
import time

fmin = 10.
fmax = 512.
deltaF = 1./8.

######## load in data from file


data_file = np.column_stack( np.loadtxt("H1-freqDataWithInj.dat") )
data = data_file[1] + 1j*data_file[2]
psd_file = np.column_stack( np.loadtxt("H1-PSD.dat") )
psd = psd_file[1]
# data and psd start at 0Hz: remove data and psd below fmin
fmin_index = int(fmin/deltaF)
data = data[fmin_index:len(data)]
psd = psd[fmin_index:len(psd)]


ntemps = 16
nwalkers = 500
ndim = 9
nsteps = 1000


def mc_eta_to_m1m2(mc, eta):
  # note m1 >= m2
  if eta <= 0.25 and eta > 0.:
	root = np.sqrt(0.25-eta)
  	fraction = (0.5+root) / (0.5-root)
  	m1 = mc * (pow(1+1.0/fraction,0.2) / pow(1.0/fraction,0.6))
  	m2 = mc * (pow(1+fraction,0.2) / pow(fraction,0.6))
  	return m1, m2
  else:
	return 1., 500.
 
def htilde_of_f(fmin, fmax, deltaF, m1, m2, chi1L, chi2L, chip, thetaJ, alpha, dist, phi_c):

        fref = 20.
        dist = 1e6*lal.lal.PC_SI*dist

        H = lalsimulation.SimIMRPhenomP(chi1L, chi2L, chip, thetaJ,
        m1, m2, dist, alpha, 0, deltaF, fmin, fmax, fref, 1, None)

        fmin_index = int(fmin / deltaF) # waveform generator computes waveforms from
                                        # 0Hz, so use this to get the wavefrom from fmin
        hplus = H[0].data.data[fmin_index:len(H[0].data.data)]
        hcross = H[1].data.data[fmin_index:len(H[1].data.data)]
	#fseries = np.linspace(fmin, fmax, (fmax-fmin)/deltaF + 1)

        return (hplus + hcross)*np.exp(1j*np.pi*2.*phi_c)

def LALInferenceCubeToFlatPrior(r, x1, x2):

	return x1 + r * ( x2 - x1 );


def logPrior(x):


	mc = x[0]
        eta = x[1]
	chi1L = x[2]
	chi2L = x[3]
	chip = x[4]
	thetaJ = x[5]
        alpha = x[6]
        dist = x[7]
	phi_c = x[8]
	
        m1, m2 = mc_eta_to_m1m2(mc, eta)
        q = m1/m2

	if mc >= mc_min and mc <= mc_max and q <= q_max and q >= q_min and dist <= dist_max and dist >= dist_min and alpha >= angle_min and alpha <= angle_max and thetaJ >= angle_min and thetaJ <= angle_max and chi1L >= chi_min and chi1L <= chi_max and chi2L >= chi_min and chi2L <= chi_max and chip >= chi_min and chip <= chi_max and phi_c <= angle_max and phi_c >= angle_min:	

		logprior = np.log(((m1+m2)*(m1+m2))/((m1-m2)*pow(eta,3.0/5.0)) )	#mc, eta contribution	
	
		return logprior
	else:
		return -np.inf

def logL(x, data, psd, fmin, fmax, deltaF):

        mc = x[0]
        eta = x[1]
	chi1L = x[2]
        chi2L = x[3]
        chip = x[4]
	thetaJ = x[5]
	alpha = x[6]
	dist = x[7]
	phi_c = x[8]

	m1, m2 = mc_eta_to_m1m2(mc, eta)

        q = m1/m2

	#if mc >= 13. and mc <= 40. and q <= 9. and q >= 1. and np.sqrt(chi1L**2 + chip**2) <= 1 and dist <= 1500 and dist >= 500 and alpha >= -np.pi*2 and alpha <= np.pi*2 and thetaJ >= -np.pi*2 and thetaJ <= np.pi*2 and chi1L >= -1 and chi1L <= 1 and chi2L >= -1 and chi2L <= 1 and chip >= -1 and chip <= 1:
	if mc >= mc_min and mc <= mc_max and q <= q_max and q >= q_min and dist <= dist_max and dist >= dist_min and alpha >= angle_min and alpha <= angle_max and thetaJ >= angle_min and thetaJ <= angle_max and chi1L >= chi_min and chi1L <= chi_max and chi2L >= chi_min and chi2L <= chi_max and chip >= chi_min and chip <= chi_max and phi_c <= angle_max and phi_c >= angle_min:

		

        	m1 *= lal.lal.MSUN_SI
        	m2 *= lal.lal.MSUN_SI
	
	        htilde = htilde_of_f(fmin, fmax, deltaF, m1, m2, chi1L, chi2L, chip, thetaJ, alpha, dist, phi_c)
	
	        logL = -0.5 * ( 4*deltaF*np.vdot(data - htilde, (data - htilde)/psd)).real 

		return logL

	else:
		return -np.inf

eta_min, eta_max = 0.09, 0.25
q_min, q_max = 1., 9.
mc_min, mc_max = 13., 40.
chi_min, chi_max = 0, 1.
angle_min, angle_max = 0., np.pi*2.
dist_min, dist_max = 500., 1500

eta0 = np.random.uniform(low=eta_min, high=eta_max, size=(ntemps, nwalkers, 1))
mc0 =  np.random.uniform(low=mc_min, high=mc_max, size=(ntemps, nwalkers, 1))
chi1L =  np.random.uniform(low=chi_min, high=chi_max, size=(ntemps, nwalkers, 1))
chi2L =  np.random.uniform(low=chi_min, high=chi_max, size=(ntemps, nwalkers, 1))
chip =  np.random.uniform(low=chi_min, high=chi_max, size=(ntemps, nwalkers, 1))
alpha = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))
thetaJ = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))
dist = np.random.uniform(low=dist_min, high=dist_max, size=(ntemps, nwalkers, 1))
phi_c = np.random.uniform(low=angle_min, high=angle_max, size=(ntemps, nwalkers, 1))


p0 = np.array([mc0, eta0, chi1L, chi2L, chip, alpha, thetaJ, dist, phi_c])
p0 = np.reshape(p0, (ndim,ntemps, nwalkers))
p0 = np.swapaxes(p0, 2,1)
p0 = np.swapaxes(p0, 0,2)

betas = np.logspace(0, -ntemps, ntemps, base=10)

pool = MPIPool(loadbalance=True)

if not pool.is_master():
    pool.wait()
    sys.exit(0)

sampler = PTSampler(ntemps, nwalkers, ndim, logL, logPrior, loglargs=[data, psd, fmin, fmax, deltaF], betas=betas, pool=pool)
t1 = time.time()
(pos, lnprob, rstate) = sampler.run_mcmc(p0, nsteps)
#all_nans = np.isnan(lnprob)
#lnprob[all_nans] = -15
print np.isnan(lnprob).any()
t2=time.time()
pool.close()
(lnZ_pt, dlnZ_pt) = sampler.thermodynamic_integration_log_evidence(fburnin=0.5)
print "lnZ_pt = {} +/- {}".format(lnZ_pt, dlnZ_pt)
print "BF = {} ".format(lnZ_pt + 0.5*4*deltaF*np.vdot(data,data/psd))

print t2-t1

import matplotlib
matplotlib.use('Agg')

import corner
samples = sampler.chain[0]
samples = samples[:, 500:, :].reshape(-1, ndim)
fig = corner.corner(samples, labels=["$m_c$", "$\eta$", "chi1L", "chi2L", "chip", "alpha", "thetaJ", "dist", "phi_c"],
                      )
fig.savefig("triangle.png")

