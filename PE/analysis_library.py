import numpy as np
import lalsimulation
import lal

def mc_eta_to_m1m2(mc, eta):
  # note m1 >= m2
  root = np.sqrt(0.25-eta)
  fraction = (0.5+root) / (0.5-root)
  m1 = mc * (pow(1+1.0/fraction,0.2) / pow(1.0/fraction,0.6))
  m2 = mc * (pow(1+fraction,0.2) / pow(fraction,0.6))
  return m1, m2

def htilde_of_f(fmin, fmax, deltaF, m1, m2):

        fref = 20
        chi1L = 0
        chi2L = 0
	chip = 0
        thetaJ = 0
        alpha = 0
        dist = 1e6*lal.lal.PC_SI*1000

        H = lalsimulation.SimIMRPhenomP(chi1L, chi2L, chip, thetaJ,
        m1, m2, dist, alpha, 0, deltaF, fmin, fmax, fref, 1, None)

        fmin_index = int(fmin / deltaF) # waveform generator computes waveforms from
                                        # 0Hz, so use this to get the wavefrom from fmin

        hplus = H[0].data.data[fmin_index:len(H[0].data.data)]
        hcross = H[1].data.data[fmin_index:len(H[1].data.data)]

        return hplus + hcross

def lnprob(x, data, psd, fmin, fmax, deltaF):

        mc = x[0]
        eta = x[1]
	m1, m2 = mc_eta_to_m1m2(mc, eta)

	q = m1/m2

        if mc >= 13 and mc <= 40 and q <= 9 and q >= 1:

		m1 *= lal.lal.MSUN_SI
		m2 *= lal.lal.MSUN_SI

		htilde = htilde_of_f(fmin, fmax, deltaF, m1, m2)
	
		logL = -0.5 * ( 4*deltaF*np.vdot(data - htilde, (data - htilde)/psd) - 4*deltaF*np.vdot(data, data/psd) ).real

									      		# this just removes the 	
											# the large constant

		logp = logL
		return logp

        else:

                return -np.inf

