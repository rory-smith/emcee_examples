import emcee
from analysis_library import *

fmin = 10
fmax = 512
deltaF = 1./8.

######## load in data from file


data_file = np.column_stack( np.loadtxt("H1-freqDataWithInj.dat") )
data = data_file[1] + 1j*data_file[2]
psd_file = np.column_stack( np.loadtxt("H1-PSD.dat") )
psd = psd_file[1]
# data and psd start at 0Hz: remove data and psd below fmin
fmin_index = int(fmin/deltaF)
data = data[fmin_index:len(data)]
psd = psd[fmin_index:len(psd)]

########


####### set up the mcmc

ndim, nwalkers = 2, 100
p0 = [ [np.random.uniform(13, 40), np.random.uniform(0.1, 0.25)] for i in range(nwalkers)]
sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, args=[data, psd, fmin, fmax, deltaF], threads=1)
# start the burnin
pos, prob, state = sampler.run_mcmc(p0, 100)
# start the sampler
sampler.reset() # resetting and starting the sampler again like this treats the first sampling as burnin 
sampler.run_mcmc(p0, 1000)

#pickle.dump( sampler.flatchain, open( "posterior_samples.p", "wb" ) )

import matplotlib.pyplot as pl
param_list = ['mc', 'eta']
true_values = [28.59, 0.248]
for i in range(ndim):
    pl.figure()
    pl.hist(sampler.flatchain[:,i][::int(sampler.acor[i])], 100, color="k", histtype="step")
    #pl.hist(sampler.flatchain[:,i], 100, color="k", histtype="step")
    pl.axvline(true_values[i])
    pl.title("%s"%(param_list[i]))
    pl.savefig("%s.png"%param_list[i])
    pl.show()
